<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "baranginfo.php" ?>
<?php include_once "pegawaiinfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$barang_delete = NULL; // Initialize page object first

class cbarang_delete extends cbarang {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{0A13A1BE-E88F-4F38-8DDC-EC4517B34576}";

	// Table name
	var $TableName = 'barang';

	// Page object name
	var $PageObjName = 'barang_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}
    var $AuditTrailOnAdd = FALSE;
    var $AuditTrailOnEdit = FALSE;
    var $AuditTrailOnDelete = TRUE;
    var $AuditTrailOnView = FALSE;
    var $AuditTrailOnViewData = FALSE;
    var $AuditTrailOnSearch = FALSE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (barang)
		if (!isset($GLOBALS["barang"]) || get_class($GLOBALS["barang"]) == "cbarang") {
			$GLOBALS["barang"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["barang"];
		}

		// Table object (pegawai)
		if (!isset($GLOBALS['pegawai'])) $GLOBALS['pegawai'] = new cpegawai();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'barang', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (pegawai)
		if (!isset($UserTable)) {
			$UserTable = new cpegawai();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanDelete()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("baranglist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $barang;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($barang);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("baranglist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in barang class, baranginfo.php

		$this->CurrentFilter = $sFilter;

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} else {
			$this->CurrentAction = "I"; // Display record
		}
		switch ($this->CurrentAction) {
			case "D": // Delete
				$this->SendEmail = TRUE; // Send email on delete success
				if ($this->DeleteRows()) { // Delete rows
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
					$this->Page_Terminate($this->getReturnUrl()); // Return to caller
				}
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->ID_BARANG->setDbValue($rs->fields('ID_BARANG'));
		$this->NAMA_BARANG->setDbValue($rs->fields('NAMA_BARANG'));
		$this->JENIS_BARANG->setDbValue($rs->fields('JENIS_BARANG'));
		$this->JUMLAH_BARANG->setDbValue($rs->fields('JUMLAH_BARANG'));
		$this->HARGA_JUAL->setDbValue($rs->fields('HARGA_JUAL'));
		$this->HARGA_BELI->setDbValue($rs->fields('HARGA_BELI'));
		$this->Av_Supplier->setDbValue($rs->fields('Av_Supplier'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->ID_BARANG->DbValue = $row['ID_BARANG'];
		$this->NAMA_BARANG->DbValue = $row['NAMA_BARANG'];
		$this->JENIS_BARANG->DbValue = $row['JENIS_BARANG'];
		$this->JUMLAH_BARANG->DbValue = $row['JUMLAH_BARANG'];
		$this->HARGA_JUAL->DbValue = $row['HARGA_JUAL'];
		$this->HARGA_BELI->DbValue = $row['HARGA_BELI'];
		$this->Av_Supplier->DbValue = $row['Av_Supplier'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->JUMLAH_BARANG->FormValue == $this->JUMLAH_BARANG->CurrentValue && is_numeric(ew_StrToFloat($this->JUMLAH_BARANG->CurrentValue)))
			$this->JUMLAH_BARANG->CurrentValue = ew_StrToFloat($this->JUMLAH_BARANG->CurrentValue);

		// Convert decimal values if posted back
		if ($this->HARGA_JUAL->FormValue == $this->HARGA_JUAL->CurrentValue && is_numeric(ew_StrToFloat($this->HARGA_JUAL->CurrentValue)))
			$this->HARGA_JUAL->CurrentValue = ew_StrToFloat($this->HARGA_JUAL->CurrentValue);

		// Convert decimal values if posted back
		if ($this->HARGA_BELI->FormValue == $this->HARGA_BELI->CurrentValue && is_numeric(ew_StrToFloat($this->HARGA_BELI->CurrentValue)))
			$this->HARGA_BELI->CurrentValue = ew_StrToFloat($this->HARGA_BELI->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// ID_BARANG
		// NAMA_BARANG
		// JENIS_BARANG
		// JUMLAH_BARANG
		// HARGA_JUAL
		// HARGA_BELI
		// Av_Supplier

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// ID_BARANG
		$this->ID_BARANG->ViewValue = $this->ID_BARANG->CurrentValue;
		$this->ID_BARANG->ViewCustomAttributes = "";

		// NAMA_BARANG
		$this->NAMA_BARANG->ViewValue = $this->NAMA_BARANG->CurrentValue;
		$this->NAMA_BARANG->ViewCustomAttributes = "";

		// JENIS_BARANG
		if (strval($this->JENIS_BARANG->CurrentValue) <> "") {
			$this->JENIS_BARANG->ViewValue = $this->JENIS_BARANG->OptionCaption($this->JENIS_BARANG->CurrentValue);
		} else {
			$this->JENIS_BARANG->ViewValue = NULL;
		}
		$this->JENIS_BARANG->ViewCustomAttributes = "";

		// JUMLAH_BARANG
		$this->JUMLAH_BARANG->ViewValue = $this->JUMLAH_BARANG->CurrentValue;
		$this->JUMLAH_BARANG->ViewCustomAttributes = "";

		// HARGA_JUAL
		$this->HARGA_JUAL->ViewValue = $this->HARGA_JUAL->CurrentValue;
		$this->HARGA_JUAL->ViewCustomAttributes = "";

		// HARGA_BELI
		$this->HARGA_BELI->ViewValue = $this->HARGA_BELI->CurrentValue;
		$this->HARGA_BELI->ViewCustomAttributes = "";

		// Av_Supplier
		if (strval($this->Av_Supplier->CurrentValue) <> "") {
			$arwrk = explode(",", $this->Av_Supplier->CurrentValue);
			$sFilterWrk = "";
			foreach ($arwrk as $wrk) {
				if ($sFilterWrk <> "") $sFilterWrk .= " OR ";
				$sFilterWrk .= "`NAMA_SUPPLIER`" . ew_SearchString("=", trim($wrk), EW_DATATYPE_STRING, "");
			}
		$sSqlWrk = "SELECT `NAMA_SUPPLIER`, `NAMA_SUPPLIER` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `supplier`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->Av_Supplier, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$this->Av_Supplier->ViewValue = "";
				$ari = 0;
				while (!$rswrk->EOF) {
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->Av_Supplier->ViewValue .= $this->Av_Supplier->DisplayValue($arwrk);
					$rswrk->MoveNext();
					if (!$rswrk->EOF) $this->Av_Supplier->ViewValue .= ew_ViewOptionSeparator($ari); // Separate Options
					$ari++;
				}
				$rswrk->Close();
			} else {
				$this->Av_Supplier->ViewValue = $this->Av_Supplier->CurrentValue;
			}
		} else {
			$this->Av_Supplier->ViewValue = NULL;
		}
		$this->Av_Supplier->ViewCustomAttributes = "";

			// ID_BARANG
			$this->ID_BARANG->LinkCustomAttributes = "";
			$this->ID_BARANG->HrefValue = "";
			$this->ID_BARANG->TooltipValue = "";

			// NAMA_BARANG
			$this->NAMA_BARANG->LinkCustomAttributes = "";
			$this->NAMA_BARANG->HrefValue = "";
			$this->NAMA_BARANG->TooltipValue = "";

			// JENIS_BARANG
			$this->JENIS_BARANG->LinkCustomAttributes = "";
			$this->JENIS_BARANG->HrefValue = "";
			$this->JENIS_BARANG->TooltipValue = "";

			// JUMLAH_BARANG
			$this->JUMLAH_BARANG->LinkCustomAttributes = "";
			$this->JUMLAH_BARANG->HrefValue = "";
			$this->JUMLAH_BARANG->TooltipValue = "";

			// HARGA_JUAL
			$this->HARGA_JUAL->LinkCustomAttributes = "";
			$this->HARGA_JUAL->HrefValue = "";
			$this->HARGA_JUAL->TooltipValue = "";

			// HARGA_BELI
			$this->HARGA_BELI->LinkCustomAttributes = "";
			$this->HARGA_BELI->HrefValue = "";
			$this->HARGA_BELI->TooltipValue = "";

			// Av_Supplier
			$this->Av_Supplier->LinkCustomAttributes = "";
			$this->Av_Supplier->HrefValue = "";
			$this->Av_Supplier->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		$conn->BeginTrans();
		if ($this->AuditTrailOnDelete) $this->WriteAuditTrailDummy($Language->Phrase("BatchDeleteBegin")); // Batch delete begin

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['ID_BARANG'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
			if ($DeleteRows) {
				foreach ($rsold as $row)
					$this->WriteAuditTrailOnDelete($row);
			}
			if ($this->AuditTrailOnDelete) $this->WriteAuditTrailDummy($Language->Phrase("BatchDeleteSuccess")); // Batch delete success
		} else {
			$conn->RollbackTrans(); // Rollback changes
			if ($this->AuditTrailOnDelete) $this->WriteAuditTrailDummy($Language->Phrase("BatchDeleteRollback")); // Batch delete rollback
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "baranglist.php", "", $this->TableVar, TRUE);
		$PageId = "delete";
		$Breadcrumb->Add("delete", $PageId, $url);
	}

	// Write Audit Trail start/end for grid update
	function WriteAuditTrailDummy($typ) {
		$table = 'barang';
		$usr = CurrentUserID();
		ew_WriteAuditTrail("log", ew_StdCurrentDateTime(), ew_ScriptName(), $usr, $typ, $table, "", "", "", "");
	}

	// Write Audit Trail (delete page)
	function WriteAuditTrailOnDelete(&$rs) {
		global $Language;
		if (!$this->AuditTrailOnDelete) return;
		$table = 'barang';

		// Get key value
		$key = "";
		if ($key <> "")
			$key .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
		$key .= $rs['ID_BARANG'];

		// Write Audit Trail
		$dt = ew_StdCurrentDateTime();
		$id = ew_ScriptName();
		$curUser = CurrentUserID();
		foreach (array_keys($rs) as $fldname) {
			if (array_key_exists($fldname, $this->fields) && $this->fields[$fldname]->FldDataType <> EW_DATATYPE_BLOB) { // Ignore BLOB fields
				if ($this->fields[$fldname]->FldHtmlTag == "PASSWORD") {
					$oldvalue = $Language->Phrase("PasswordMask"); // Password Field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_MEMO) {
					if (EW_AUDIT_TRAIL_TO_DATABASE)
						$oldvalue = $rs[$fldname];
					else
						$oldvalue = "[MEMO]"; // Memo field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_XML) {
					$oldvalue = "[XML]"; // XML field
				} else {
					$oldvalue = $rs[$fldname];
				}
				ew_WriteAuditTrail("log", $dt, $id, $curUser, "D", $table, $fldname, $key, $oldvalue, "");
			}
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($barang_delete)) $barang_delete = new cbarang_delete();

// Page init
$barang_delete->Page_Init();

// Page main
$barang_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$barang_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "delete";
var CurrentForm = fbarangdelete = new ew_Form("fbarangdelete", "delete");

// Form_CustomValidate event
fbarangdelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fbarangdelete.ValidateRequired = true;
<?php } else { ?>
fbarangdelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fbarangdelete.Lists["x_JENIS_BARANG"] = {"LinkField":"","Ajax":false,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fbarangdelete.Lists["x_JENIS_BARANG"].Options = <?php echo json_encode($barang->JENIS_BARANG->Options()) ?>;
fbarangdelete.Lists["x_Av_Supplier[]"] = {"LinkField":"x_NAMA_SUPPLIER","Ajax":true,"AutoFill":false,"DisplayFields":["x_NAMA_SUPPLIER","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php

// Load records for display
if ($barang_delete->Recordset = $barang_delete->LoadRecordset())
	$barang_deleteTotalRecs = $barang_delete->Recordset->RecordCount(); // Get record count
if ($barang_deleteTotalRecs <= 0) { // No record found, exit
	if ($barang_delete->Recordset)
		$barang_delete->Recordset->Close();
	$barang_delete->Page_Terminate("baranglist.php"); // Return to list
}
?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $barang_delete->ShowPageHeader(); ?>
<?php
$barang_delete->ShowMessage();
?>
<form name="fbarangdelete" id="fbarangdelete" class="form-inline ewForm ewDeleteForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($barang_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $barang_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="barang">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($barang_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="ewGrid">
<div class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="table ewTable">
<?php echo $barang->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($barang->ID_BARANG->Visible) { // ID_BARANG ?>
		<th><span id="elh_barang_ID_BARANG" class="barang_ID_BARANG"><?php echo $barang->ID_BARANG->FldCaption() ?></span></th>
<?php } ?>
<?php if ($barang->NAMA_BARANG->Visible) { // NAMA_BARANG ?>
		<th><span id="elh_barang_NAMA_BARANG" class="barang_NAMA_BARANG"><?php echo $barang->NAMA_BARANG->FldCaption() ?></span></th>
<?php } ?>
<?php if ($barang->JENIS_BARANG->Visible) { // JENIS_BARANG ?>
		<th><span id="elh_barang_JENIS_BARANG" class="barang_JENIS_BARANG"><?php echo $barang->JENIS_BARANG->FldCaption() ?></span></th>
<?php } ?>
<?php if ($barang->JUMLAH_BARANG->Visible) { // JUMLAH_BARANG ?>
		<th><span id="elh_barang_JUMLAH_BARANG" class="barang_JUMLAH_BARANG"><?php echo $barang->JUMLAH_BARANG->FldCaption() ?></span></th>
<?php } ?>
<?php if ($barang->HARGA_JUAL->Visible) { // HARGA_JUAL ?>
		<th><span id="elh_barang_HARGA_JUAL" class="barang_HARGA_JUAL"><?php echo $barang->HARGA_JUAL->FldCaption() ?></span></th>
<?php } ?>
<?php if ($barang->HARGA_BELI->Visible) { // HARGA_BELI ?>
		<th><span id="elh_barang_HARGA_BELI" class="barang_HARGA_BELI"><?php echo $barang->HARGA_BELI->FldCaption() ?></span></th>
<?php } ?>
<?php if ($barang->Av_Supplier->Visible) { // Av_Supplier ?>
		<th><span id="elh_barang_Av_Supplier" class="barang_Av_Supplier"><?php echo $barang->Av_Supplier->FldCaption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$barang_delete->RecCnt = 0;
$i = 0;
while (!$barang_delete->Recordset->EOF) {
	$barang_delete->RecCnt++;
	$barang_delete->RowCnt++;

	// Set row properties
	$barang->ResetAttrs();
	$barang->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$barang_delete->LoadRowValues($barang_delete->Recordset);

	// Render row
	$barang_delete->RenderRow();
?>
	<tr<?php echo $barang->RowAttributes() ?>>
<?php if ($barang->ID_BARANG->Visible) { // ID_BARANG ?>
		<td<?php echo $barang->ID_BARANG->CellAttributes() ?>>
<span id="el<?php echo $barang_delete->RowCnt ?>_barang_ID_BARANG" class="barang_ID_BARANG">
<span<?php echo $barang->ID_BARANG->ViewAttributes() ?>>
<?php echo $barang->ID_BARANG->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($barang->NAMA_BARANG->Visible) { // NAMA_BARANG ?>
		<td<?php echo $barang->NAMA_BARANG->CellAttributes() ?>>
<span id="el<?php echo $barang_delete->RowCnt ?>_barang_NAMA_BARANG" class="barang_NAMA_BARANG">
<span<?php echo $barang->NAMA_BARANG->ViewAttributes() ?>>
<?php echo $barang->NAMA_BARANG->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($barang->JENIS_BARANG->Visible) { // JENIS_BARANG ?>
		<td<?php echo $barang->JENIS_BARANG->CellAttributes() ?>>
<span id="el<?php echo $barang_delete->RowCnt ?>_barang_JENIS_BARANG" class="barang_JENIS_BARANG">
<span<?php echo $barang->JENIS_BARANG->ViewAttributes() ?>>
<?php echo $barang->JENIS_BARANG->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($barang->JUMLAH_BARANG->Visible) { // JUMLAH_BARANG ?>
		<td<?php echo $barang->JUMLAH_BARANG->CellAttributes() ?>>
<span id="el<?php echo $barang_delete->RowCnt ?>_barang_JUMLAH_BARANG" class="barang_JUMLAH_BARANG">
<span<?php echo $barang->JUMLAH_BARANG->ViewAttributes() ?>>
<?php echo $barang->JUMLAH_BARANG->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($barang->HARGA_JUAL->Visible) { // HARGA_JUAL ?>
		<td<?php echo $barang->HARGA_JUAL->CellAttributes() ?>>
<span id="el<?php echo $barang_delete->RowCnt ?>_barang_HARGA_JUAL" class="barang_HARGA_JUAL">
<span<?php echo $barang->HARGA_JUAL->ViewAttributes() ?>>
<?php echo $barang->HARGA_JUAL->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($barang->HARGA_BELI->Visible) { // HARGA_BELI ?>
		<td<?php echo $barang->HARGA_BELI->CellAttributes() ?>>
<span id="el<?php echo $barang_delete->RowCnt ?>_barang_HARGA_BELI" class="barang_HARGA_BELI">
<span<?php echo $barang->HARGA_BELI->ViewAttributes() ?>>
<?php echo $barang->HARGA_BELI->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($barang->Av_Supplier->Visible) { // Av_Supplier ?>
		<td<?php echo $barang->Av_Supplier->CellAttributes() ?>>
<span id="el<?php echo $barang_delete->RowCnt ?>_barang_Av_Supplier" class="barang_Av_Supplier">
<span<?php echo $barang->Av_Supplier->ViewAttributes() ?>>
<?php echo $barang->Av_Supplier->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$barang_delete->Recordset->MoveNext();
}
$barang_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $barang_delete->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
</div>
</form>
<script type="text/javascript">
fbarangdelete.Init();
</script>
<?php
$barang_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$barang_delete->Page_Terminate();
?>
