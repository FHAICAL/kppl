<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "baranginfo.php" ?>
<?php include_once "pegawaiinfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$barang_edit = NULL; // Initialize page object first

class cbarang_edit extends cbarang {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{0A13A1BE-E88F-4F38-8DDC-EC4517B34576}";

	// Table name
	var $TableName = 'barang';

	// Page object name
	var $PageObjName = 'barang_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}
    var $AuditTrailOnAdd = FALSE;
    var $AuditTrailOnEdit = TRUE;
    var $AuditTrailOnDelete = FALSE;
    var $AuditTrailOnView = FALSE;
    var $AuditTrailOnViewData = FALSE;
    var $AuditTrailOnSearch = FALSE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (barang)
		if (!isset($GLOBALS["barang"]) || get_class($GLOBALS["barang"]) == "cbarang") {
			$GLOBALS["barang"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["barang"];
		}

		// Table object (pegawai)
		if (!isset($GLOBALS['pegawai'])) $GLOBALS['pegawai'] = new cpegawai();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'barang', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (pegawai)
		if (!isset($UserTable)) {
			$UserTable = new cpegawai();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanEdit()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("baranglist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $barang;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($barang);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewEditForm";
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Load key from QueryString
		if (@$_GET["ID_BARANG"] <> "") {
			$this->ID_BARANG->setQueryStringValue($_GET["ID_BARANG"]);
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->ID_BARANG->CurrentValue == "")
			$this->Page_Terminate("baranglist.php"); // Invalid key, return to list

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("baranglist.php"); // No matching record, return to list
				}
				break;
			Case "U": // Update
				$sReturnUrl = $this->getReturnUrl();
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} elseif ($this->getFailureMessage() == $Language->Phrase("NoRecord")) {
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed
				}
		}

		// Render the record
		$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->ID_BARANG->FldIsDetailKey) {
			$this->ID_BARANG->setFormValue($objForm->GetValue("x_ID_BARANG"));
		}
		if (!$this->NAMA_BARANG->FldIsDetailKey) {
			$this->NAMA_BARANG->setFormValue($objForm->GetValue("x_NAMA_BARANG"));
		}
		if (!$this->JENIS_BARANG->FldIsDetailKey) {
			$this->JENIS_BARANG->setFormValue($objForm->GetValue("x_JENIS_BARANG"));
		}
		if (!$this->JUMLAH_BARANG->FldIsDetailKey) {
			$this->JUMLAH_BARANG->setFormValue($objForm->GetValue("x_JUMLAH_BARANG"));
		}
		if (!$this->HARGA_JUAL->FldIsDetailKey) {
			$this->HARGA_JUAL->setFormValue($objForm->GetValue("x_HARGA_JUAL"));
		}
		if (!$this->HARGA_BELI->FldIsDetailKey) {
			$this->HARGA_BELI->setFormValue($objForm->GetValue("x_HARGA_BELI"));
		}
		if (!$this->Av_Supplier->FldIsDetailKey) {
			$this->Av_Supplier->setFormValue($objForm->GetValue("x_Av_Supplier"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->ID_BARANG->CurrentValue = $this->ID_BARANG->FormValue;
		$this->NAMA_BARANG->CurrentValue = $this->NAMA_BARANG->FormValue;
		$this->JENIS_BARANG->CurrentValue = $this->JENIS_BARANG->FormValue;
		$this->JUMLAH_BARANG->CurrentValue = $this->JUMLAH_BARANG->FormValue;
		$this->HARGA_JUAL->CurrentValue = $this->HARGA_JUAL->FormValue;
		$this->HARGA_BELI->CurrentValue = $this->HARGA_BELI->FormValue;
		$this->Av_Supplier->CurrentValue = $this->Av_Supplier->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->ID_BARANG->setDbValue($rs->fields('ID_BARANG'));
		$this->NAMA_BARANG->setDbValue($rs->fields('NAMA_BARANG'));
		$this->JENIS_BARANG->setDbValue($rs->fields('JENIS_BARANG'));
		$this->JUMLAH_BARANG->setDbValue($rs->fields('JUMLAH_BARANG'));
		$this->HARGA_JUAL->setDbValue($rs->fields('HARGA_JUAL'));
		$this->HARGA_BELI->setDbValue($rs->fields('HARGA_BELI'));
		$this->Av_Supplier->setDbValue($rs->fields('Av_Supplier'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->ID_BARANG->DbValue = $row['ID_BARANG'];
		$this->NAMA_BARANG->DbValue = $row['NAMA_BARANG'];
		$this->JENIS_BARANG->DbValue = $row['JENIS_BARANG'];
		$this->JUMLAH_BARANG->DbValue = $row['JUMLAH_BARANG'];
		$this->HARGA_JUAL->DbValue = $row['HARGA_JUAL'];
		$this->HARGA_BELI->DbValue = $row['HARGA_BELI'];
		$this->Av_Supplier->DbValue = $row['Av_Supplier'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->JUMLAH_BARANG->FormValue == $this->JUMLAH_BARANG->CurrentValue && is_numeric(ew_StrToFloat($this->JUMLAH_BARANG->CurrentValue)))
			$this->JUMLAH_BARANG->CurrentValue = ew_StrToFloat($this->JUMLAH_BARANG->CurrentValue);

		// Convert decimal values if posted back
		if ($this->HARGA_JUAL->FormValue == $this->HARGA_JUAL->CurrentValue && is_numeric(ew_StrToFloat($this->HARGA_JUAL->CurrentValue)))
			$this->HARGA_JUAL->CurrentValue = ew_StrToFloat($this->HARGA_JUAL->CurrentValue);

		// Convert decimal values if posted back
		if ($this->HARGA_BELI->FormValue == $this->HARGA_BELI->CurrentValue && is_numeric(ew_StrToFloat($this->HARGA_BELI->CurrentValue)))
			$this->HARGA_BELI->CurrentValue = ew_StrToFloat($this->HARGA_BELI->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// ID_BARANG
		// NAMA_BARANG
		// JENIS_BARANG
		// JUMLAH_BARANG
		// HARGA_JUAL
		// HARGA_BELI
		// Av_Supplier

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// ID_BARANG
		$this->ID_BARANG->ViewValue = $this->ID_BARANG->CurrentValue;
		$this->ID_BARANG->ViewCustomAttributes = "";

		// NAMA_BARANG
		$this->NAMA_BARANG->ViewValue = $this->NAMA_BARANG->CurrentValue;
		$this->NAMA_BARANG->ViewCustomAttributes = "";

		// JENIS_BARANG
		if (strval($this->JENIS_BARANG->CurrentValue) <> "") {
			$this->JENIS_BARANG->ViewValue = $this->JENIS_BARANG->OptionCaption($this->JENIS_BARANG->CurrentValue);
		} else {
			$this->JENIS_BARANG->ViewValue = NULL;
		}
		$this->JENIS_BARANG->ViewCustomAttributes = "";

		// JUMLAH_BARANG
		$this->JUMLAH_BARANG->ViewValue = $this->JUMLAH_BARANG->CurrentValue;
		$this->JUMLAH_BARANG->ViewCustomAttributes = "";

		// HARGA_JUAL
		$this->HARGA_JUAL->ViewValue = $this->HARGA_JUAL->CurrentValue;
		$this->HARGA_JUAL->ViewCustomAttributes = "";

		// HARGA_BELI
		$this->HARGA_BELI->ViewValue = $this->HARGA_BELI->CurrentValue;
		$this->HARGA_BELI->ViewCustomAttributes = "";

		// Av_Supplier
		if (strval($this->Av_Supplier->CurrentValue) <> "") {
			$arwrk = explode(",", $this->Av_Supplier->CurrentValue);
			$sFilterWrk = "";
			foreach ($arwrk as $wrk) {
				if ($sFilterWrk <> "") $sFilterWrk .= " OR ";
				$sFilterWrk .= "`NAMA_SUPPLIER`" . ew_SearchString("=", trim($wrk), EW_DATATYPE_STRING, "");
			}
		$sSqlWrk = "SELECT `NAMA_SUPPLIER`, `NAMA_SUPPLIER` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `supplier`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->Av_Supplier, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$this->Av_Supplier->ViewValue = "";
				$ari = 0;
				while (!$rswrk->EOF) {
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->Av_Supplier->ViewValue .= $this->Av_Supplier->DisplayValue($arwrk);
					$rswrk->MoveNext();
					if (!$rswrk->EOF) $this->Av_Supplier->ViewValue .= ew_ViewOptionSeparator($ari); // Separate Options
					$ari++;
				}
				$rswrk->Close();
			} else {
				$this->Av_Supplier->ViewValue = $this->Av_Supplier->CurrentValue;
			}
		} else {
			$this->Av_Supplier->ViewValue = NULL;
		}
		$this->Av_Supplier->ViewCustomAttributes = "";

			// ID_BARANG
			$this->ID_BARANG->LinkCustomAttributes = "";
			$this->ID_BARANG->HrefValue = "";
			$this->ID_BARANG->TooltipValue = "";

			// NAMA_BARANG
			$this->NAMA_BARANG->LinkCustomAttributes = "";
			$this->NAMA_BARANG->HrefValue = "";
			$this->NAMA_BARANG->TooltipValue = "";

			// JENIS_BARANG
			$this->JENIS_BARANG->LinkCustomAttributes = "";
			$this->JENIS_BARANG->HrefValue = "";
			$this->JENIS_BARANG->TooltipValue = "";

			// JUMLAH_BARANG
			$this->JUMLAH_BARANG->LinkCustomAttributes = "";
			$this->JUMLAH_BARANG->HrefValue = "";
			$this->JUMLAH_BARANG->TooltipValue = "";

			// HARGA_JUAL
			$this->HARGA_JUAL->LinkCustomAttributes = "";
			$this->HARGA_JUAL->HrefValue = "";
			$this->HARGA_JUAL->TooltipValue = "";

			// HARGA_BELI
			$this->HARGA_BELI->LinkCustomAttributes = "";
			$this->HARGA_BELI->HrefValue = "";
			$this->HARGA_BELI->TooltipValue = "";

			// Av_Supplier
			$this->Av_Supplier->LinkCustomAttributes = "";
			$this->Av_Supplier->HrefValue = "";
			$this->Av_Supplier->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// ID_BARANG
			$this->ID_BARANG->EditAttrs["class"] = "form-control";
			$this->ID_BARANG->EditCustomAttributes = "";
			$this->ID_BARANG->EditValue = $this->ID_BARANG->CurrentValue;
			$this->ID_BARANG->ViewCustomAttributes = "";

			// NAMA_BARANG
			$this->NAMA_BARANG->EditAttrs["class"] = "form-control";
			$this->NAMA_BARANG->EditCustomAttributes = "";
			$this->NAMA_BARANG->EditValue = ew_HtmlEncode($this->NAMA_BARANG->CurrentValue);
			$this->NAMA_BARANG->PlaceHolder = ew_RemoveHtml($this->NAMA_BARANG->FldCaption());

			// JENIS_BARANG
			$this->JENIS_BARANG->EditCustomAttributes = "";
			$this->JENIS_BARANG->EditValue = $this->JENIS_BARANG->Options(FALSE);

			// JUMLAH_BARANG
			$this->JUMLAH_BARANG->EditAttrs["class"] = "form-control";
			$this->JUMLAH_BARANG->EditCustomAttributes = "";
			$this->JUMLAH_BARANG->EditValue = ew_HtmlEncode($this->JUMLAH_BARANG->CurrentValue);
			$this->JUMLAH_BARANG->PlaceHolder = ew_RemoveHtml($this->JUMLAH_BARANG->FldCaption());
			if (strval($this->JUMLAH_BARANG->EditValue) <> "" && is_numeric($this->JUMLAH_BARANG->EditValue)) $this->JUMLAH_BARANG->EditValue = ew_FormatNumber($this->JUMLAH_BARANG->EditValue, -2, -1, -2, 0);

			// HARGA_JUAL
			$this->HARGA_JUAL->EditAttrs["class"] = "form-control";
			$this->HARGA_JUAL->EditCustomAttributes = "";
			$this->HARGA_JUAL->EditValue = ew_HtmlEncode($this->HARGA_JUAL->CurrentValue);
			$this->HARGA_JUAL->PlaceHolder = ew_RemoveHtml($this->HARGA_JUAL->FldCaption());
			if (strval($this->HARGA_JUAL->EditValue) <> "" && is_numeric($this->HARGA_JUAL->EditValue)) $this->HARGA_JUAL->EditValue = ew_FormatNumber($this->HARGA_JUAL->EditValue, -2, -1, -2, 0);

			// HARGA_BELI
			$this->HARGA_BELI->EditAttrs["class"] = "form-control";
			$this->HARGA_BELI->EditCustomAttributes = "";
			$this->HARGA_BELI->EditValue = ew_HtmlEncode($this->HARGA_BELI->CurrentValue);
			$this->HARGA_BELI->PlaceHolder = ew_RemoveHtml($this->HARGA_BELI->FldCaption());
			if (strval($this->HARGA_BELI->EditValue) <> "" && is_numeric($this->HARGA_BELI->EditValue)) $this->HARGA_BELI->EditValue = ew_FormatNumber($this->HARGA_BELI->EditValue, -2, -1, -2, 0);

			// Av_Supplier
			$this->Av_Supplier->EditCustomAttributes = "";
			if (trim(strval($this->Av_Supplier->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$arwrk = explode(",", $this->Av_Supplier->CurrentValue);
				$sFilterWrk = "";
				foreach ($arwrk as $wrk) {
					if ($sFilterWrk <> "") $sFilterWrk .= " OR ";
					$sFilterWrk .= "`NAMA_SUPPLIER`" . ew_SearchString("=", trim($wrk), EW_DATATYPE_STRING, "");
				}
			}
			$sSqlWrk = "SELECT `NAMA_SUPPLIER`, `NAMA_SUPPLIER` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `supplier`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->Av_Supplier, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->Av_Supplier->EditValue = $arwrk;

			// Edit refer script
			// ID_BARANG

			$this->ID_BARANG->HrefValue = "";

			// NAMA_BARANG
			$this->NAMA_BARANG->HrefValue = "";

			// JENIS_BARANG
			$this->JENIS_BARANG->HrefValue = "";

			// JUMLAH_BARANG
			$this->JUMLAH_BARANG->HrefValue = "";

			// HARGA_JUAL
			$this->HARGA_JUAL->HrefValue = "";

			// HARGA_BELI
			$this->HARGA_BELI->HrefValue = "";

			// Av_Supplier
			$this->Av_Supplier->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->ID_BARANG->FldIsDetailKey && !is_null($this->ID_BARANG->FormValue) && $this->ID_BARANG->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->ID_BARANG->FldCaption(), $this->ID_BARANG->ReqErrMsg));
		}
		if (!$this->NAMA_BARANG->FldIsDetailKey && !is_null($this->NAMA_BARANG->FormValue) && $this->NAMA_BARANG->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->NAMA_BARANG->FldCaption(), $this->NAMA_BARANG->ReqErrMsg));
		}
		if ($this->JENIS_BARANG->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->JENIS_BARANG->FldCaption(), $this->JENIS_BARANG->ReqErrMsg));
		}
		if (!$this->JUMLAH_BARANG->FldIsDetailKey && !is_null($this->JUMLAH_BARANG->FormValue) && $this->JUMLAH_BARANG->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->JUMLAH_BARANG->FldCaption(), $this->JUMLAH_BARANG->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->JUMLAH_BARANG->FormValue)) {
			ew_AddMessage($gsFormError, $this->JUMLAH_BARANG->FldErrMsg());
		}
		if (!$this->HARGA_JUAL->FldIsDetailKey && !is_null($this->HARGA_JUAL->FormValue) && $this->HARGA_JUAL->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->HARGA_JUAL->FldCaption(), $this->HARGA_JUAL->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->HARGA_JUAL->FormValue)) {
			ew_AddMessage($gsFormError, $this->HARGA_JUAL->FldErrMsg());
		}
		if (!$this->HARGA_BELI->FldIsDetailKey && !is_null($this->HARGA_BELI->FormValue) && $this->HARGA_BELI->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->HARGA_BELI->FldCaption(), $this->HARGA_BELI->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->HARGA_BELI->FormValue)) {
			ew_AddMessage($gsFormError, $this->HARGA_BELI->FldErrMsg());
		}
		if ($this->Av_Supplier->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->Av_Supplier->FldCaption(), $this->Av_Supplier->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// ID_BARANG
			// NAMA_BARANG

			$this->NAMA_BARANG->SetDbValueDef($rsnew, $this->NAMA_BARANG->CurrentValue, "", $this->NAMA_BARANG->ReadOnly);

			// JENIS_BARANG
			$this->JENIS_BARANG->SetDbValueDef($rsnew, $this->JENIS_BARANG->CurrentValue, "", $this->JENIS_BARANG->ReadOnly);

			// JUMLAH_BARANG
			$this->JUMLAH_BARANG->SetDbValueDef($rsnew, $this->JUMLAH_BARANG->CurrentValue, 0, $this->JUMLAH_BARANG->ReadOnly);

			// HARGA_JUAL
			$this->HARGA_JUAL->SetDbValueDef($rsnew, $this->HARGA_JUAL->CurrentValue, 0, $this->HARGA_JUAL->ReadOnly);

			// HARGA_BELI
			$this->HARGA_BELI->SetDbValueDef($rsnew, $this->HARGA_BELI->CurrentValue, 0, $this->HARGA_BELI->ReadOnly);

			// Av_Supplier
			$this->Av_Supplier->SetDbValueDef($rsnew, $this->Av_Supplier->CurrentValue, "", $this->Av_Supplier->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		if ($EditRow) {
			$this->WriteAuditTrailOnEdit($rsold, $rsnew);
		}
		$rs->Close();
		return $EditRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "baranglist.php", "", $this->TableVar, TRUE);
		$PageId = "edit";
		$Breadcrumb->Add("edit", $PageId, $url);
	}

	// Write Audit Trail start/end for grid update
	function WriteAuditTrailDummy($typ) {
		$table = 'barang';
		$usr = CurrentUserID();
		ew_WriteAuditTrail("log", ew_StdCurrentDateTime(), ew_ScriptName(), $usr, $typ, $table, "", "", "", "");
	}

	// Write Audit Trail (edit page)
	function WriteAuditTrailOnEdit(&$rsold, &$rsnew) {
		global $Language;
		if (!$this->AuditTrailOnEdit) return;
		$table = 'barang';

		// Get key value
		$key = "";
		if ($key <> "") $key .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
		$key .= $rsold['ID_BARANG'];

		// Write Audit Trail
		$dt = ew_StdCurrentDateTime();
		$id = ew_ScriptName();
		$usr = CurrentUserID();
		foreach (array_keys($rsnew) as $fldname) {
			if ($this->fields[$fldname]->FldDataType <> EW_DATATYPE_BLOB) { // Ignore BLOB fields
				if ($this->fields[$fldname]->FldDataType == EW_DATATYPE_DATE) { // DateTime field
					$modified = (ew_FormatDateTime($rsold[$fldname], 0) <> ew_FormatDateTime($rsnew[$fldname], 0));
				} else {
					$modified = !ew_CompareValue($rsold[$fldname], $rsnew[$fldname]);
				}
				if ($modified) {
					if ($this->fields[$fldname]->FldHtmlTag == "PASSWORD") { // Password Field
						$oldvalue = $Language->Phrase("PasswordMask");
						$newvalue = $Language->Phrase("PasswordMask");
					} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_MEMO) { // Memo field
						if (EW_AUDIT_TRAIL_TO_DATABASE) {
							$oldvalue = $rsold[$fldname];
							$newvalue = $rsnew[$fldname];
						} else {
							$oldvalue = "[MEMO]";
							$newvalue = "[MEMO]";
						}
					} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_XML) { // XML field
						$oldvalue = "[XML]";
						$newvalue = "[XML]";
					} else {
						$oldvalue = $rsold[$fldname];
						$newvalue = $rsnew[$fldname];
					}
					ew_WriteAuditTrail("log", $dt, $id, $usr, "U", $table, $fldname, $key, $oldvalue, $newvalue);
				}
			}
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($barang_edit)) $barang_edit = new cbarang_edit();

// Page init
$barang_edit->Page_Init();

// Page main
$barang_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$barang_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "edit";
var CurrentForm = fbarangedit = new ew_Form("fbarangedit", "edit");

// Validate form
fbarangedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_ID_BARANG");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $barang->ID_BARANG->FldCaption(), $barang->ID_BARANG->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_NAMA_BARANG");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $barang->NAMA_BARANG->FldCaption(), $barang->NAMA_BARANG->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_JENIS_BARANG");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $barang->JENIS_BARANG->FldCaption(), $barang->JENIS_BARANG->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_JUMLAH_BARANG");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $barang->JUMLAH_BARANG->FldCaption(), $barang->JUMLAH_BARANG->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_JUMLAH_BARANG");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($barang->JUMLAH_BARANG->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_HARGA_JUAL");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $barang->HARGA_JUAL->FldCaption(), $barang->HARGA_JUAL->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_HARGA_JUAL");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($barang->HARGA_JUAL->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_HARGA_BELI");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $barang->HARGA_BELI->FldCaption(), $barang->HARGA_BELI->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_HARGA_BELI");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($barang->HARGA_BELI->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_Av_Supplier[]");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $barang->Av_Supplier->FldCaption(), $barang->Av_Supplier->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fbarangedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fbarangedit.ValidateRequired = true;
<?php } else { ?>
fbarangedit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fbarangedit.Lists["x_JENIS_BARANG"] = {"LinkField":"","Ajax":false,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fbarangedit.Lists["x_JENIS_BARANG"].Options = <?php echo json_encode($barang->JENIS_BARANG->Options()) ?>;
fbarangedit.Lists["x_Av_Supplier[]"] = {"LinkField":"x_NAMA_SUPPLIER","Ajax":true,"AutoFill":false,"DisplayFields":["x_NAMA_SUPPLIER","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $barang_edit->ShowPageHeader(); ?>
<?php
$barang_edit->ShowMessage();
?>
<form name="fbarangedit" id="fbarangedit" class="<?php echo $barang_edit->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($barang_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $barang_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="barang">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<div>
<?php if ($barang->ID_BARANG->Visible) { // ID_BARANG ?>
	<div id="r_ID_BARANG" class="form-group">
		<label id="elh_barang_ID_BARANG" for="x_ID_BARANG" class="col-sm-2 control-label ewLabel"><?php echo $barang->ID_BARANG->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $barang->ID_BARANG->CellAttributes() ?>>
<span id="el_barang_ID_BARANG">
<span<?php echo $barang->ID_BARANG->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $barang->ID_BARANG->EditValue ?></p></span>
</span>
<input type="hidden" data-table="barang" data-field="x_ID_BARANG" name="x_ID_BARANG" id="x_ID_BARANG" value="<?php echo ew_HtmlEncode($barang->ID_BARANG->CurrentValue) ?>">
<?php echo $barang->ID_BARANG->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($barang->NAMA_BARANG->Visible) { // NAMA_BARANG ?>
	<div id="r_NAMA_BARANG" class="form-group">
		<label id="elh_barang_NAMA_BARANG" for="x_NAMA_BARANG" class="col-sm-2 control-label ewLabel"><?php echo $barang->NAMA_BARANG->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $barang->NAMA_BARANG->CellAttributes() ?>>
<span id="el_barang_NAMA_BARANG">
<input type="text" data-table="barang" data-field="x_NAMA_BARANG" name="x_NAMA_BARANG" id="x_NAMA_BARANG" size="30" maxlength="50" placeholder="<?php echo ew_HtmlEncode($barang->NAMA_BARANG->getPlaceHolder()) ?>" value="<?php echo $barang->NAMA_BARANG->EditValue ?>"<?php echo $barang->NAMA_BARANG->EditAttributes() ?>>
</span>
<?php echo $barang->NAMA_BARANG->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($barang->JENIS_BARANG->Visible) { // JENIS_BARANG ?>
	<div id="r_JENIS_BARANG" class="form-group">
		<label id="elh_barang_JENIS_BARANG" class="col-sm-2 control-label ewLabel"><?php echo $barang->JENIS_BARANG->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $barang->JENIS_BARANG->CellAttributes() ?>>
<span id="el_barang_JENIS_BARANG">
<div id="tp_x_JENIS_BARANG" class="ewTemplate"><input type="radio" data-table="barang" data-field="x_JENIS_BARANG" data-value-separator="<?php echo ew_HtmlEncode(is_array($barang->JENIS_BARANG->DisplayValueSeparator) ? json_encode($barang->JENIS_BARANG->DisplayValueSeparator) : $barang->JENIS_BARANG->DisplayValueSeparator) ?>" name="x_JENIS_BARANG" id="x_JENIS_BARANG" value="{value}"<?php echo $barang->JENIS_BARANG->EditAttributes() ?>></div>
<div id="dsl_x_JENIS_BARANG" data-repeatcolumn="5" class="ewItemList"><div>
<?php
$arwrk = $barang->JENIS_BARANG->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($barang->JENIS_BARANG->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="barang" data-field="x_JENIS_BARANG" name="x_JENIS_BARANG" id="x_JENIS_BARANG_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $barang->JENIS_BARANG->EditAttributes() ?>><?php echo $barang->JENIS_BARANG->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($barang->JENIS_BARANG->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="barang" data-field="x_JENIS_BARANG" name="x_JENIS_BARANG" id="x_JENIS_BARANG_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($barang->JENIS_BARANG->CurrentValue) ?>" checked<?php echo $barang->JENIS_BARANG->EditAttributes() ?>><?php echo $barang->JENIS_BARANG->CurrentValue ?></label>
<?php
    }
}
?>
</div></div>
</span>
<?php echo $barang->JENIS_BARANG->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($barang->JUMLAH_BARANG->Visible) { // JUMLAH_BARANG ?>
	<div id="r_JUMLAH_BARANG" class="form-group">
		<label id="elh_barang_JUMLAH_BARANG" for="x_JUMLAH_BARANG" class="col-sm-2 control-label ewLabel"><?php echo $barang->JUMLAH_BARANG->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $barang->JUMLAH_BARANG->CellAttributes() ?>>
<span id="el_barang_JUMLAH_BARANG">
<input type="text" data-table="barang" data-field="x_JUMLAH_BARANG" name="x_JUMLAH_BARANG" id="x_JUMLAH_BARANG" size="30" placeholder="<?php echo ew_HtmlEncode($barang->JUMLAH_BARANG->getPlaceHolder()) ?>" value="<?php echo $barang->JUMLAH_BARANG->EditValue ?>"<?php echo $barang->JUMLAH_BARANG->EditAttributes() ?>>
</span>
<?php echo $barang->JUMLAH_BARANG->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($barang->HARGA_JUAL->Visible) { // HARGA_JUAL ?>
	<div id="r_HARGA_JUAL" class="form-group">
		<label id="elh_barang_HARGA_JUAL" for="x_HARGA_JUAL" class="col-sm-2 control-label ewLabel"><?php echo $barang->HARGA_JUAL->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $barang->HARGA_JUAL->CellAttributes() ?>>
<span id="el_barang_HARGA_JUAL">
<input type="text" data-table="barang" data-field="x_HARGA_JUAL" name="x_HARGA_JUAL" id="x_HARGA_JUAL" size="30" placeholder="<?php echo ew_HtmlEncode($barang->HARGA_JUAL->getPlaceHolder()) ?>" value="<?php echo $barang->HARGA_JUAL->EditValue ?>"<?php echo $barang->HARGA_JUAL->EditAttributes() ?>>
</span>
<?php echo $barang->HARGA_JUAL->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($barang->HARGA_BELI->Visible) { // HARGA_BELI ?>
	<div id="r_HARGA_BELI" class="form-group">
		<label id="elh_barang_HARGA_BELI" for="x_HARGA_BELI" class="col-sm-2 control-label ewLabel"><?php echo $barang->HARGA_BELI->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $barang->HARGA_BELI->CellAttributes() ?>>
<span id="el_barang_HARGA_BELI">
<input type="text" data-table="barang" data-field="x_HARGA_BELI" name="x_HARGA_BELI" id="x_HARGA_BELI" size="30" placeholder="<?php echo ew_HtmlEncode($barang->HARGA_BELI->getPlaceHolder()) ?>" value="<?php echo $barang->HARGA_BELI->EditValue ?>"<?php echo $barang->HARGA_BELI->EditAttributes() ?>>
</span>
<?php echo $barang->HARGA_BELI->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($barang->Av_Supplier->Visible) { // Av_Supplier ?>
	<div id="r_Av_Supplier" class="form-group">
		<label id="elh_barang_Av_Supplier" class="col-sm-2 control-label ewLabel"><?php echo $barang->Av_Supplier->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $barang->Av_Supplier->CellAttributes() ?>>
<span id="el_barang_Av_Supplier">
<div id="tp_x_Av_Supplier" class="ewTemplate"><input type="checkbox" data-table="barang" data-field="x_Av_Supplier" data-value-separator="<?php echo ew_HtmlEncode(is_array($barang->Av_Supplier->DisplayValueSeparator) ? json_encode($barang->Av_Supplier->DisplayValueSeparator) : $barang->Av_Supplier->DisplayValueSeparator) ?>" name="x_Av_Supplier[]" id="x_Av_Supplier[]" value="{value}"<?php echo $barang->Av_Supplier->EditAttributes() ?>></div>
<div id="dsl_x_Av_Supplier" data-repeatcolumn="5" class="ewItemList"><div>
<?php
$arwrk = $barang->Av_Supplier->EditValue;
if (is_array($arwrk)) {
	$armultiwrk= explode(",", strval($barang->Av_Supplier->CurrentValue));
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = "";
		$cnt = count($armultiwrk);
		for ($ari = 0; $ari < $cnt; $ari++) {
			if (strval($arwrk[$rowcntwrk][0]) == trim(strval($armultiwrk[$ari]))) {
				unset($armultiwrk[$ari]); // Remove already matched item
				$selwrk = " checked";
				if ($selwrk <> "") $emptywrk = FALSE;
				break;
			}
		}
		if ($selwrk <> "") {
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="checkbox-inline"><input type="checkbox" data-table="barang" data-field="x_Av_Supplier" name="x_Av_Supplier[]" id="x_Av_Supplier_<?php echo $rowcntwrk ?>[]" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $barang->Av_Supplier->EditAttributes() ?>><?php echo $barang->Av_Supplier->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
		}
	}
	$rowswrk = count($armultiwrk);
	if ($rowswrk > 0) {
		for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
?>
<label class="checkbox-inline"><input type="checkbox" data-table="barang" data-field="x_Av_Supplier" name="x_Av_Supplier[]" value="<?php echo ew_HtmlEncode($armultiwrk[$rowcntwrk]) ?>" checked<?php echo $barang->Av_Supplier->EditAttributes() ?>><?php echo $armultiwrk[$rowcntwrk] ?></label>
<?php
		}
	}
}
?>
</div></div>
<?php
$sSqlWrk = "SELECT `NAMA_SUPPLIER`, `NAMA_SUPPLIER` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `supplier`";
$sWhereWrk = "";
$barang->Av_Supplier->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$barang->Av_Supplier->LookupFilters += array("f0" => "`NAMA_SUPPLIER` = {filter_value}", "t0" => "200", "fn0" => "");
$sSqlWrk = "";
$barang->Lookup_Selecting($barang->Av_Supplier, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $barang->Av_Supplier->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_Av_Supplier" id="s_x_Av_Supplier" value="<?php echo $barang->Av_Supplier->LookupFilterQuery() ?>">
</span>
<?php echo $barang->Av_Supplier->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("SaveBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $barang_edit->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
</form>
<script type="text/javascript">
fbarangedit.Init();
</script>
<?php
$barang_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$barang_edit->Page_Terminate();
?>
